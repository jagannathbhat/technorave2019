window.onload = () => {
	let events = [
		{ name: "Blackhat Cybersecurity Workshop", date: "3" },
		{ name: "Blogging Workshop", date: "3" },
		{ name: "Hardware Assembly", date: "3" },
		{ name: "Introduction to Blockchain", date: "3" },
		{ name: "Quiz of Thrones", date: "3" },
		{ name: "Best Techie", date: "3" },
		{ name: "LAN Gaming", date: "3, 4" },
		{ name: "Best Manager", date: "4" },
		{ name: "Web Design with Google Firebase", date: "4" },
		{ name: "Adobe Creative Workshop", date: "4" },
		{ name: "Adobe Premiere Pro Workshop", date: "4" },
		{ name: "Tech Quiz", date: "4" },
		{ name: "Treasure Hunt", date: "4" },
	];
	for (i = 0; i < events.length; i++) {
		card = document.createElement("div");
		card.classList.add("jb-card");
		img = document.createElement("img");
		img.setAttribute("src", "event" + i + ".jpg");
		h4 = document.createElement("h4");
		h4.innerText = events[i].name;
		p = document.createElement("p");
		p.innerText = "May " + events[i].date;
		card.appendChild(img);
		card.appendChild(h4);
		card.appendChild(p);
		document.getElementById("jb-events").appendChild(card);
	}
};
