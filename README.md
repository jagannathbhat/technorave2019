# TechnoRave 2019

This is the official website for TechnoRave 2019, an event organized by the CS-IT department of Adi Shankara Institute of Engineering and Technology.

The website was made in a hurry. The event organizers did not plan on maintaining a website for the event. However, a few days before the event, the need for a website was eveident. I managed to patch together this website in a couple of hours and hosted it on Firebase at [technorave2019.web.app](https://technorave2019.web.app/). It was hosted on Firebase because the topic for a workshop I conducted at the event was "Web Design with Google Firebase".

This repository contains a slightly modified version of the original website and it can be previewed at [jagannathbhat.gitlab.io/technorave2019](https://jagannathbhat.gitlab.io/technorave2019)